Drupal.behaviors.rickroll = {
  attach: function (context, settings) {
    var rolltime = 2000;
    if (Drupal.settings.rickroll.rolltime) {
      rolltime = parseInt(Drupal.settings.rickroll.rolltime) * 1000;
    }
    setTimeout(function() {
      window.location.href = "http://www.youtube.com/watch?v=oHg5SJYRHA0";
    }, rolltime);
  }
}
