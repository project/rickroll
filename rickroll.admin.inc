<?php
/**
 * Administer RickRoll Settings
 */

/**
 * Menu callback used for admin/config/content/rickroll
 */
function rickroll_admin_settings() {
  //load node types, because we know it's
  //important to control how you roll...yeah right.

  $options = array();
  $node_types = node_type_get_types();
  foreach($node_types as $node_type){
    $options[$node_type->type] = $node_type->name;
  }
  
  $form['rickroll_node_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('What node type should be RickRoll-able?'),
    '#options' => $options,
    '#default_value' => variable_get('rickroll_node_types', array()),
    '#description' => t("Types You Can Roll'em From")
    
  );
  
  return system_settings_form($form);
}
